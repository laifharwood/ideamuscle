//
//  IdeaGlobal.swift
//  IdeaMuscle
//
//  Created by Laif Harwood on 6/29/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import Foundation
import Parse
import UIKit
import ParseUI


func ideaQueryGlobal(daysInPast: Int, query: PFQuery){
    
    query.orderByAscending("createdAt")
    query.orderByDescending("numberOfUpvotes")
    query.limit = 100
    query.whereKey("isPublic", equalTo: true)
    query.includeKey("owner")
    //query.includeKey("usersWhoUpvoted")
    query.includeKey("topicPointer")
    query.cachePolicy = PFCachePolicy.NetworkElseCache
    
    getIdeasToHideGlobal(query)

    if daysInPast < 0{
    let now = NSDate()
    let calendar = NSCalendar.currentCalendar()
    let timeZone = NSTimeZone(abbreviation: "GMT")
    calendar.timeZone = timeZone!
    let components = NSDateComponents()
    components.day = daysInPast
    let dateInPast = calendar.dateByAddingComponents(components, toDate: now, options: [])
    query.whereKey("createdAt", greaterThanOrEqualTo: dateInPast!)
    }
}

func getIdeasToHideGlobal(query: PFQuery){
    if let currentUser = PFUser.currentUser(){
        if let ideasToHideId = currentUser["ideasToHideId"] as? [String]{
            query.whereKey("objectId", notContainedIn: ideasToHideId)
        }
        if let topicsToHide = currentUser["topicsToHide"] as? [PFObject]{
            query.whereKey("topicPointer", notContainedIn: topicsToHide)
        }
    }
}



func tableViewIdeaConfig(tableView: UITableView){
    
    tableView.rowHeight = 150
    tableView.contentInset = UIEdgeInsetsMake(0, 0, 80, 0)
    tableView.registerClass(IdeaTableViewCell.self, forCellReuseIdentifier: "Cell")
    
}

func longPressToTableViewGlobal(sender: AnyObject, tableView: UITableView, reportViewContainer: UIView){
    let longGest = UILongPressGestureRecognizer()
    longGest.addTarget(sender, action: "cellLongPress:")
    longGest.minimumPressDuration = 0.4
    tableView.addGestureRecognizer(longGest)
    reportViewContainer.frame = CGRectMake(0, sender.tabBarController!!.tabBar.frame.maxY, sender.view!.frame.width, 100)
}

func cellLongPressGlobal(sender: UILongPressGestureRecognizer, tableView: UITableView, senderSelf: AnyObject, reportViewContainer: UIView, invisibleView: UIView){
    let point = sender.locationInView(tableView)
    let indexPath = tableView.indexPathForRowAtPoint(point)
    
    senderSelf.tabBarController!!.tabBar.hidden = true
    
    UIView.animateWithDuration(0.5, animations: { () -> Void in
        reportViewContainer.frame = CGRectMake(0, senderSelf.tabBarController!!.tabBar.frame.maxY - 140, senderSelf.view!.frame.width, 140)
        reportViewContainer.backgroundColor = UIColor.whiteColor()
        senderSelf.navigationController!!.view.addSubview(reportViewContainer)
        senderSelf.view!.bringSubviewToFront(reportViewContainer)
    })
    
    let hideButton = UIButton(frame: CGRectMake(5, 5, senderSelf.view!.frame.width - 10, 40))
    let hideAndReportButton = UIButton(frame: CGRectMake(5, hideButton.frame.maxY + 5, senderSelf.view!.frame.width - 10, 40))
    let cancelHideButton = UIButton(frame: CGRectMake(5, hideAndReportButton.frame.maxY + 5, senderSelf.view!.frame.width - 10, 40))
    
    hideButton.setTitle("Hide", forState: .Normal)
    hideAndReportButton.setTitle("Hide And Report", forState: .Normal)
    cancelHideButton.setTitle("Cancel", forState: .Normal)
    
    hideButton.backgroundColor = fiftyGrayColor
    hideAndReportButton.backgroundColor = fiftyGrayColor
    cancelHideButton.backgroundColor = twoHundredGrayColor
    
    hideButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    hideAndReportButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    cancelHideButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    
    hideButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 15)
    hideAndReportButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 15)
    cancelHideButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 15)
    
    hideButton.addTarget(senderSelf, action: "hideIdea:", forControlEvents: .TouchUpInside)
    hideAndReportButton.addTarget(senderSelf, action: "hideAndReport:", forControlEvents: .TouchUpInside)
    cancelHideButton.addTarget(senderSelf, action: "cancelHide:", forControlEvents: .TouchUpInside)
    
    hideButton.tag = indexPath!.row
    hideAndReportButton.tag = indexPath!.row
    
    hideButton.layer.cornerRadius = 3
    hideAndReportButton.layer.cornerRadius = 3
    cancelHideButton.layer.cornerRadius = 3
    
    reportViewContainer.addSubview(hideButton)
    reportViewContainer.addSubview(hideAndReportButton)
    reportViewContainer.addSubview(cancelHideButton)
    
    invisibleView.frame = CGRectMake(0, UIApplication.sharedApplication().statusBarFrame.height, senderSelf.view!.frame.width, reportViewContainer.frame.minY - UIApplication.sharedApplication().statusBarFrame.height)
    invisibleView.backgroundColor = oneFiftyGrayColor
    invisibleView.alpha = 0.7
    senderSelf.navigationController!!.view.addSubview(invisibleView)
}

func hideInvisibleAndReportView(invisibleView: UIView, sender: AnyObject, reportViewContainer: UIView){
    
    invisibleView.removeFromSuperview()
    
    UIView.animateWithDuration(0.5, animations: { () -> Void in
        
        reportViewContainer.frame = CGRectMake(0, sender.tabBarController!!.tabBar.frame.maxY, sender.view!.frame.width, 100)
        sender.tabBarController!!.tabBar.hidden = false
    })
}

func hideAndReportGlobal(ideaObjects: [PFObject], sender: UIButton, senderSelf: AnyObject, hideIdea: (UIButton) -> ()){
    let reportVC = ReportAbuseViewController()
    reportVC.activeIdea = ideaObjects[sender.tag]
    if let topic = ideaObjects[sender.tag]["topicPointer"] as? PFObject{
        reportVC.activeTopic = topic
        reportVC.isFromTopic == false
        senderSelf.navigationController?!.pushViewController(reportVC, animated: true)
    }
    hideIdea(sender)
}


func cellFrame(cell: UITableViewCell, view: UIView){
    
    cell.frame = CGRectMake(0, 0, view.frame.width, 150)
}

func topicLabelGlobal(labelWidth: CGFloat, topicLabel: UILabel, ideaObjects: [PFObject], row: Int){
topicLabel.frame = CGRectMake(10, 5, labelWidth, 20)
topicLabel.font = UIFont(name: "Avenir-Heavy", size: 14)
topicLabel.numberOfLines = 1
topicLabel.textColor = UIColor.blackColor()
    
    if ideaObjects[row]["topicPointer"] != nil{
        let topic = ideaObjects[row]["topicPointer"] as! PFObject
        if let text = topic["title"] as? String{
            topicLabel.text = text
        }
    }
    
topicLabel.tag = row
}

func ideaLabelGlobal(labelWidth: CGFloat, ideaLabel: UILabel, ideaObjects: [PFObject], row: Int, topicLabel: UILabel){
    ideaLabel.numberOfLines = 0
    ideaLabel.frame = CGRectMake(25, topicLabel.frame.maxY + 2, labelWidth - 15, 70)
    ideaLabel.font = UIFont(name: "Avenir-Light", size: 14)
    ideaLabel.textColor = oneFiftyGrayColor
    if ideaObjects[row]["content"] != nil{
        ideaLabel.text = (ideaObjects[row]["content"] as! String)
    }
}

func getUpvotes(idea: PFObject, button: UIButton, cell: UITableViewCell?) -> Bool{
    var numberOfUpvotes = Int()
    var hasUpvoted = Bool()
    numberOfUpvotes = idea["numberOfUpvotes"] as! Int
    let numberString = abbreviateNumber(numberOfUpvotes)
    button.setTitle(numberString as String, forState: .Normal)
    if let currentUser = PFUser.currentUser(){
        if let ideasUpvoted = currentUser["ideasUpvoted"] as? [String]{
            
            if let ideaId = idea.objectId{
                if ideasUpvoted.contains(ideaId){
                    button.setTitleColor(redColor, forState: .Normal)
                    button.tintColor = redColor
                    hasUpvoted = true
                }else{
                    button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                    button.tintColor = UIColor.whiteColor()
                    hasUpvoted = false
                }
            }else{
                button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                button.tintColor = UIColor.whiteColor()
                hasUpvoted = false
            }
        }else{
            button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            button.tintColor = UIColor.whiteColor()
            hasUpvoted = false
        }
        if cell != nil{
            button.frame =  CGRectMake(cell!.frame.maxX - (40) - 10, cell!.frame.height/2 - 30, 40, 60)
        }
        let image = UIImage(named: "upvoteArrow")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 10)
        button.setImage(image, forState: .Normal)
        _ = CGFloat(20)
        let imageSize = button.imageView!.image!.size
        let titleSize = button.titleLabel!.frame.size
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -imageSize.width, 0, 0)
        button.imageEdgeInsets = UIEdgeInsetsMake(-45, 0.0, 0.0, -titleSize.width)
        button.layer.cornerRadius = 3
        button.backgroundColor = seventySevenGrayColor
        
    }else{
        button.setTitle("0", forState: .Normal)
        button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    }
    
    if hasUpvoted == true{
        return true
    }else{
        return false
    }
}

func profileButtonGlobal(ideaObjects: [PFObject], row: Int, profileButton: PFImageView){
    
    var pfUser = PFUser()
    if ideaObjects[row]["owner"] != nil{
        pfUser = ideaObjects[row]["owner"] as! PFUser
        if PFUser.currentUser() == pfUser{
            profileButton.layer.borderColor = redColor.CGColor
            profileButton.layer.borderWidth = 2
        }else{
            profileButton.layer.borderColor = UIColor.whiteColor().CGColor
            profileButton.layer.borderWidth = 0
        }
        
        getAvatar(pfUser, imageView: nil, parseImageView: profileButton)
        
    }
    profileButton.tag = row
    profileButton.userInteractionEnabled = true
    profileButton.frame = CGRectMake(10, 105, 40, 40)
    profileButton.layer.cornerRadius = 20
    profileButton.layer.masksToBounds = true
}

func usernameGlobal(usernameLabel: UILabel, row: Int, ideaObjects: [PFObject], profileButton: PFImageView){
    var usernameLabelWidth = CGFloat()
    usernameLabelWidth = 190
    usernameLabel.frame = CGRectMake(profileButton.frame.maxX + 2, profileButton.frame.maxY - profileButton.frame.height/2, usernameLabelWidth, 20)
    usernameLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 12)
    usernameLabel.textColor = twoHundredGrayColor
    if ideaObjects[row]["owner"] != nil{
        let user = ideaObjects[row]["owner"] as! PFUser
        let username = user["username"] as! String
        usernameLabel.text = username
    }
    usernameLabel.tag = row
}

func timeStampGlobal(ideaObjects: [PFObject], timeStamp: UILabel, row: Int, usernameLabel: UILabel, cell: UITableViewCell){
    var createdAt = NSDate()
    
    if ideaObjects[row].createdAt != nil{
        createdAt = ideaObjects[row].createdAt!
        timeStamp.text = createdAt.timeAgoSimple
    }
    timeStamp.frame = CGRectMake(cell.frame.maxX - 60, usernameLabel.frame.minY, 50, 20)
    timeStamp.font = UIFont(name: "Avenir", size: 10)
    timeStamp.textColor = oneFiftyGrayColor
    timeStamp.textAlignment = NSTextAlignment.Right
}

func startActivityGlobal(activityIndicatorContainer: UIView, activityIndicator: UIActivityIndicatorView, view: UIView){
    activityIndicatorContainer.frame = CGRectMake(0, 0, view.frame.width, 50)
    activityIndicatorContainer.backgroundColor = UIColor.whiteColor()
    activityIndicatorContainer.hidden = false
    view.addSubview(activityIndicatorContainer)
    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
    activityIndicator.hidesWhenStopped = true
    activityIndicator.frame = CGRectMake(0, 0, view.frame.width, 1000)
    activityIndicator.center = activityIndicatorContainer.center
    activityIndicatorContainer.addSubview(activityIndicator)
    activityIndicator.startAnimating()
}

func composeFromDetail(sender: AnyObject!, activeTopic: PFObject?, isNewTopic: Bool){
    if isNewTopic == false{
        presentCompose(sender, activeTopic: activeTopic!, isNewTopic: isNewTopic)
    }else{
        presentCompose(sender, activeTopic: nil, isNewTopic: isNewTopic)
    }
}

func presentCompose(sender: AnyObject!, activeTopic: PFObject?, isNewTopic: Bool){
    
    if isNewTopic == false{
        let composeVC = ComposeViewController()
        composeVC.activeComposeTopicObject = activeTopic!
        sender.presentViewController(composeVC, animated: true, completion: nil)
    }else{
        let composeTopicVC = ComposeTopicViewController()
        sender.presentViewController(composeTopicVC, animated: true, completion: nil)
    }
}


func hideFilterGlobal(scrollView: UIScrollView, pointNow: CGPoint, tableSelection: UIView, periodSelection: UIView, tableView: UITableView, view: UIView, shownHeight: CGFloat, navigationController: UINavigationController, hiddenHeight: CGFloat){
    if scrollView.contentOffset.y < pointNow.y{
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            tableSelection.hidden = false
            periodSelection.hidden = false
            tableView.frame = CGRectMake(0, periodSelection.frame.maxY, view.frame.width, shownHeight)
        })
        
    }else if scrollView.contentOffset.y > pointNow.y{
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            tableSelection.hidden = true
            periodSelection.hidden = true
            tableView.frame = CGRectMake(0, navigationController.navigationBar.frame.maxY, view.frame.width, hiddenHeight)
        })
    }
}
