[gif of IdeaMuscle in action](http://gph.is/1YTk6c9)

IdeaMuscle is an IOS Swift app I made that is a Social Network built on the idea of coming up with 10 ideas a day on a certain topic in order to exercise your mind and get better at having good ideas about anything.

This app was on the app store for a short time but unfortunately I used Parse.com as a back end and when Facebook announced they were shutting down Parse.com, I decided to remove the app from the app store since it hadn't gained very much traction and also found the underlying DBMS(MongoDB) to be flawed for this type of application due to its lack of ACID compliance which caused data corruption in my database.

Despite my poor choice in a back end solution, I highly enjoyed making this app and this is the project that I have spent the most time on in my short coding experience.  Some of the features implemented in this app include:

* Social features such as the ability to follow other users, up vote ideas, comment on ideas, and share ideas to other social networks.

* In App Purchases to upgrade to pro version.  This was later removed and the full version of the app was free for everyone.

* Push notifications.

* Post private or public ideas.

* World/Friend rankings based on number of up votes.

* Browse top ideas and topics with different time filters.

* Drafts.

* User Search.

* Smart links for sharing to other social networks.